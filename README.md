# TrekBike


Assignment TrekBikes_Ameet Thakkar [08.05.2019]

**Implementation Steps**
1. Application in use (.apk) file from. Installed via ADB locally on the Galaxy S9 real device.

**Tools and Environments user**
1) Test script- Written in Java
___
2) Java Appium Client- v7.1.0 installed
___
3) JDK(1.8.X) installed
___
4) IntelliJ version 2019.2 installed
___
5) Maven v3.6.1 installed
___

3) The AUT performed on locally connected real device Samsung Galaxy S9 (OS 9.0)
___

C) Test Script Architecture
**Local Script:**
a)Intellij Java Script is created by the name "TrekRD.java" that is based on TESTNG framework, which is executed by "testng.xml" runner.
To execute or play this runner, a local appium setup( appium GUI server should be started ) and device connected locally via USB to the system.
b) Right click the testng.xml in intellij and select the "Run" Button for the test to execute and it will load the appium & device capabilities and run through the script, launch the application, check the  text is displayed, verify it and then finally close the app and also quit the driver.
A testNG report is shown on the console with Total Test Run and other status's as  per the script.
The Test Run's successful and Script passes as per the Requirement

**Remote Script**:
The Remote Script has many dependancies. This is created in mind keeping the most widely used and preferred approach: Page Object Model with Page Factory in TestNG framework.
Steps
``1) Create a Properties file: This is key value pair properties file which has details about the App, and device details.
``2) CommonUtils: This will load the properties file, setting up the Desired Capabilities & Initiate the appium driver
___

3) Custom Listeners: These will store the methods Test Start, Test Failure, Capture Screenshot, Skipped Test etc

4) Screen Base: This is Base Class and will be used to Initiate the driver, it can also be used to create common methods in the scripts which can be used for future purpose
___
5) Test Base: This file has the methods which will be called to Initiate the driver and the BeforeSuite and AfterSuite methods which will help to initialize the DesiredCapabilities,
___
Start the Appium Server, launch the app, while at the end Close the App and Quit the driver
6) AppiumServer Utils: This file stores the path for launching the appium server programmatically, without manually starting the Appium server via the GUI.
___
7) Home Screen: This is the only screen as part of this assesment where the methods are written to identify the elements on the screen
___
8) checkText: This is the actual test case which will be run through "testng_trekBikes.xml" runner.
___
9) Additionally, tried to add the reportNG feature to capature the reports after test execution to showcase it to stakeholder & clients

Test Execution of this script results in failure, rather it does not initialize the AppiumServer programatically and therefore the flow stops without execution.
Test scripting and logic is implemented but it seems that there is some problem with server starting and so troubleshooting time is needed.
This script is robust  with bare minimum changes to run on any device (android or ios) by just changing the properties file and entering the device capabilities.
___
10) Next steps: 
___
a)  Remote script <if in working condition> pushed to bitbucket and create a jenkins job and associate the repository details and run the job will execute and run the script.

