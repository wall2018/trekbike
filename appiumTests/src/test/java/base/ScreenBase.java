/* This Base Class is used to Inititate the driver
* This can be used to create some more common methods in this script- So can be used in future*/


package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ScreenBase {

    public  AppiumDriver<MobileElement> driver;

    //Create a Constructor and pass the Argument of AppiumDriver

    public ScreenBase (AppiumDriver<MobileElement> driver){

        this.driver= driver;

    }
}
