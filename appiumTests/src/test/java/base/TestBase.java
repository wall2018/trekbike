/* This contains common methods and starting, inititalizing, properties of Android driver.
This is the Test case to be executed. Need to initialze the base class for it to get executed.*/

package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utilities.AppiumServer;
import utilities.CommonUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

public class TestBase {

    //Creating the Reference for Appium Driver
    public static  AppiumDriver<MobileElement> driver;

    //load the properties file into the test case

    public static String loadPropertyFile = "Android_TrekBikes.properties";

    // Generate Logs for the test cases
   public static Logger log =Logger.getLogger("devpinoyLogger");


    @BeforeSuite
        public void setUp() throws IOException {

        if (driver==null) {
            try {
                AppiumServer.start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // log.info("Appium server started");

            //Depending upon the Properties file it will load the test either Android device or IOS device

            if (loadPropertyFile.startsWith("Android")) {
                CommonUtils.loadAndroidConfProp(loadPropertyFile);

                //set the Android Capabilities

                CommonUtils.setAndroidCapabilities();
                driver = CommonUtils.getAndroidDriver();

            } else if (loadPropertyFile.startsWith("IOS")) {

                    /*
                     CommonUtils.loadIOSConfProp(loadPropertyFile);
                    //set the Android Capabilities
                    CommonUtils.setIOSCapabilities();
                    driver = CommonUtils.getIOSDriver();
                     */
            }
        }
    }
    // After the Test case execution is completed.
    @AfterSuite
    public void tearDown() {

        driver.closeApp();
        driver.quit();

        //Stop the Appium Server Service

        AppiumServer.stop();
       // log.info("Appium Server Stopped");

    }



}
