package listeners;

import org.testng.*;

public class CustomListeners implements ITestListener, ISuiteListener {

    public void onTestStart(ITestResult iTestResult) {

        System.out.print("Starting the Test Suite: ");

    }

    public void onTestSuccess(ITestResult iTestResult) {

    }

    public void onTestFailure(ITestResult iTestResult) {

        System.out.print("Screenshot captured");
        System.setProperty("org.uncommons.reporting.escape-output","false");
        Reporter.log("Screenshot Captured");
    }

    public void onTestSkipped(ITestResult iTestResult) {

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }
    public void onStart(ITestContext iTestContext) {

    }

    public void onFinish(ITestContext iTestContext) {

        System.out.print("Ending the Test Suite");

    }

    public void onStart(ISuite iSuite) {

        System.out.print("Starting");
    }

        public void onFinish(ISuite iSuite) {

        System.out.print("Finish");
    }
}
