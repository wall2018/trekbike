// This is the first screen landing page of the TrekBike Application

package screens.TrekBikes;

import base.ScreenBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class HomeScreen extends ScreenBase {


    @AndroidFindBy(className="android.widget.TextView")
    public WebElement bodytext;



    public  HomeScreen(AppiumDriver<MobileElement> driver) {

        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);

    }

    public boolean textDisplayed(){

     return bodytext.isDisplayed();

    }

    public boolean verifyText(String expectedText){

        //String expectedText = "Believe in bikes";

        String actualText = bodytext.getText();
        Assert.assertEquals(actualText, expectedText);

        return true;

    }
}
