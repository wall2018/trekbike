//This is simple program and it works - test passes here
// It uses Real device connected to my laptop and Appium server is started Manually.

package testCases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.Assert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;


public class TrekRD {
	
	//Initialize the Appium Driver
    AppiumDriver<MobileElement> driver;
	
    // Before Test Annotation will run the method before any 'test' method.
	/*This file is used for loading all the properties, setting up the Desired Capabilities
    */

	@BeforeTest
    public void InitConfig() {

		try {
			DesiredCapabilities cap = new DesiredCapabilities();

			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy J7");
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "r_and_d_1.trekbikes.com.trekrd1");
			cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "r_and_d_1.trekbikes.com.trekrd1.MainActivity");

			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		} catch(Exception exp){
			System.out.println("Cause is: "+ exp.getCause());
			System.out.println((" Message is : " +exp.getMessage()));
			exp.printStackTrace();

		}
	}

	// Actual Test Case script to a) Display the Text and b) verify if the text is "Believe in bikes"
	@Test
	
	public void testapp() {

	WebElement  bodyText= driver.findElement(By.className("android.widget.TextView"));

	String expectedText = "Believe in bikes";
	String actualText = bodyText.getText();

	try{
	if (bodyText.isDisplayed()) {

		System.out.println("Text is Displayed = " + bodyText.isDisplayed() + " and the text is " + bodyText.getText());
		Assert.assertEquals(actualText, expectedText);
			}
	}
	catch( NoSuchElementException e)
		{
			System.out.println("Body Text not displayed");
		}

	}

	@AfterTest

	public void tearDown(){

		driver.closeApp();
		driver.quit();
	}

}
