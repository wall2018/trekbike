package testCases;

import base.TestBase;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import screens.TrekBikes.HomeScreen;

public class checkText extends TestBase {

    HomeScreen home;

        @BeforeTest
        public void init(){

            home = new HomeScreen(driver);
        }

        @Test(priority = 1)
        public void  textDisplayed() {
            String expectedText = "Believe in bikes";

            if (home.textDisplayed()) {
                System.out.println("Text is Displayed= " + home.textDisplayed() + " and the text is " + home.textDisplayed());

            }

        }
        @Test(priority = 2)
        public void validateText(){

           home.verifyText("Believe in bikes");
            System.out.println("The Expected Text  matches Actual Text");

        }

}
