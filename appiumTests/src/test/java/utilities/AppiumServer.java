// This script is created with have methods to Start and Stop Appium server programmatically

package utilities;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.testng.annotations.BeforeTest;

import java.io.File;

public class AppiumServer {

   public static AppiumDriverLocalService service;


    @BeforeTest
    public static void start() throws InterruptedException {

        AppiumDriverLocalService service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe")).withAppiumJS(new File("C:\\Users\\at275j\\AppData\\Local\\Programs\\Appium\\resources\\app\\node_modules\\appium\\lib\\main.js")).withLogFile( new File (System.getProperty("user.dir")+"\\src\\test\\resources\\logs\\TrekbikeAppium.log")));
        service.start();
    }


    public static void stop(){

        if(service.isRunning()==true){
            service.stop();
            System.out.println("Appium Server is stopped");
        }

    }
   }
