/*This file is used for loading all the properties, setting up the Desired Capabilities & initialize Appium Driver
 Created a Method- Load Android Config Property.
 Created a Method- Setting up Android Capabilities
 Created a Method- Android Driver to initiate Android Driver.
*/
package utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class CommonUtils {


    public static String PLATFORM_NAME;
    public static String PLATFORM_VERSION;
    public static String AUTOMATION_NAME;
    public static String DEVICE_NAME;
    public static String APP_PATH;
    public static String APP_PACKAGE;
    public static String APP_ACTVITY;
    public static URL serverUrl;

    private static Properties prop = new Properties();
    private static DesiredCapabilities capabilities = new DesiredCapabilities();

    //Creating the Reference for Appium Driver
    private static AppiumDriver<MobileElement> driver;

    //Load the Property file

    public static void loadAndroidConfProp(String propertyFileName) throws IOException {

        FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resources\\properties\\Android_TrekBikes.properties");
        prop.load(fis);

         PLATFORM_NAME = prop.getProperty("platform.name");
         PLATFORM_VERSION =prop.getProperty("platform.version");
         AUTOMATION_NAME = prop.getProperty("automation.name");
         DEVICE_NAME = prop.getProperty("");
        APP_PATH = prop.getProperty("app");
        APP_PACKAGE = prop.getProperty("app.package");
        APP_ACTVITY = prop.getProperty("app.activity");
    }

    // Load Android Capabilities

    public static void setAndroidCapabilities(){

        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,AUTOMATION_NAME);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,DEVICE_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,PLATFORM_VERSION);

        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,APP_PACKAGE);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,APP_ACTVITY);
    }

    //Initialize the AndroidDriver
    public static AppiumDriver<MobileElement> getAndroidDriver() throws MalformedURLException {

        serverUrl =  new URL("http://127.0.0.1:62410/wd/hub");
        driver= new AndroidDriver(serverUrl, capabilities);

        return driver;
    }
}
